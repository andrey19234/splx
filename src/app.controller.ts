import { Controller, Get, Inject, UseInterceptors } from '@nestjs/common';
import { AppService } from './app.service';
import {
  CACHE_MANAGER,
  Cache,
  CacheInterceptor,
  CacheKey,
  CacheTTL,
} from '@nestjs/cache-manager';

@UseInterceptors(CacheInterceptor)
@Controller()
export class AppController {
  constructor(
    @Inject(CACHE_MANAGER) private readonly cache: Cache,
    private readonly appService: AppService,
  ) {}

  @Get()
  @CacheKey('hello')
  @CacheTTL(360000)
  getHello(): string {
    return this.appService.getHello();
  }
}
